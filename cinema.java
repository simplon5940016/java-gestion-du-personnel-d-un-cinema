import java.util.Scanner;

class cinemaApp {
    public static final int NORMAL_HOURS_WORKED_PER_WEEK = 35;
    public static final double HOURLY_RATE = 1.5;
    /* Calcul du salaire */
    static double salaireHebdo(int hoursWorked, double hourlyRates) {
        if(hoursWorked > NORMAL_HOURS_WORKED_PER_WEEK) {
        int heureSupp = hoursWorked - NORMAL_HOURS_WORKED_PER_WEEK;
        return heureSupp * hourlyRates * HOURLY_RATE + NORMAL_HOURS_WORKED_PER_WEEK * hourlyRates;
        } else {
            return hoursWorked*hourlyRates;
        }
    }

    /* Affiche les personnes correspondantes au métier choisit */
    static void searchPosition(String[] positions, String[] employeeNames, String positionSearch){

        boolean jobExist = false;
        for(int i = 0; i < positions.length; i++) {
            if(positions[i].toLowerCase().equals(positionSearch) ){
                System.out.println("Le poste correspond pour : " + employeeNames[i]);
                jobExist = true;
            } 
        }

        if(!jobExist){
            System.out.println("Aucun employé trouvé");
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); 

        String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
        int[] hoursWorked = {35, 38, 35, 38, 40};
        double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
        String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

        /* Affiche le salaire pour chaque employé */
        for (int i = 0; i < employeeNames.length; i++) {
            System.out.println(employeeNames[i] + " : " + salaireHebdo(hoursWorked[i], hourlyRates[i]));
        }

        System.out.println("Entrer le poste à rechercher :");
        String positionSearch = scanner.nextLine();  

        searchPosition(positions, employeeNames, positionSearch.toLowerCase());

        scanner.close(); 
    }
}