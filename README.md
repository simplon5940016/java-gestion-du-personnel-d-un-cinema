# Java - Gestion du personnel d'un cinéma

## Fonctionnalités

- Calcul du salaire hebdomadaire en tenant compte des heures supplémentaires.
- Recherche d'employés par poste.

### Compilation et Exécution

Pour compiler et exécuter le programme :

```bash
java -cp . cinema.java
```
